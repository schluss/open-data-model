# First pratical thoughts on paper

Note: need to put some time into [JSON-LD](https://json-ld.org/), [Atomic Data](https://atomicdata.dev/), Standard Business Reporting (SBR) format (Belastingdienst e.a.) http://www.taxonomie-viewer.nl/yeti, HDN, [Sivi](https://www.sivi.org/) and  Financieel paspoort.

## INPUT:   
Data provided by a data providing service, in the form of XML, JSON or HTML (to be scraped) 

For example:
```
<root>
 <year>2020</year>
 <child>
  <name>John Doe</name>
 </child>
 <incomes>
  <income>
   <employer>Company X</employer>
   <startDate>1-1-2018</startDate>
   <endDate></endDate>
  </income>
 </incomes>
</root>
```

Will translate to:
```
Name		Path			Description		    Type				        Format      BusinessRules   Notes
year		root.year		Year			    number
name		root.child.name	Full name		    string		
incomes		root.incomes	List of incomes		list (link to incomes[])

incomes[]:
employer	income.employer		Company name	string
startDate	income.startDate	Start date		date				        D-M-YYYY
endDate		income.endDate		End date		date				        D-M-YYYY	
```

Sidenote:
A format like an HTML page is also taken into account. Then path (see below) would be the JS Selector:

```
firstname = '#main > div.main_body > div'	
```
								

And then is stored/written down in a universally understandable way, like JSON-LD (from there on it also would be quite easy to convert into other formats):
```
{
 "@context" : "http://schema.org/OpenDataModel.json",
 "@type" : "OpenDataModel",
 "attributes":[

	{
	 "@type" : "Attribute",
	 "name" : "name",
	 "path" : "root",
	 "description":"Year",
	 "type":"string",
	 "format":"",
	 "rules" : "",
	 "notes" : ""
	},
	....
]}
```

The result is a 'database' containing all kinds of possible attributes from different data providing services. For example UWV, Belastingdienst, BKR, Mijn Overheid and so on.

## OUTPUT:   
When a data consuming service requires a specific format, a model is created for it. In order to create a model, the data consuming service looks into what attributes are made available (from INPUT) and defines some business processing rules if needed. For example: a simple model, containing Firstname and Surname, the 'root.child.name' attribute can be used as input:

```
Name 		Match 		Path			    Description 		Type	Format  BusinessRules                           Notes
firstname	name		person.firstname	Person's firstname	string		    split at first space, take first part
surname 	name		person.surname		Person's lastname	string		    split at first space, take last part
```

Note: 'Match' is the link to the provided attributes


And this is also stored/written down in the same universally way:
```
{
 "@context" : "http://schema.org/OpenDataModel.json",
 "@type" : "OpenDataModel",
 "attributes":[

	{
	 "@type" : "Attribute",
	 "name" : "firstname",
	 "match" : "name"
	 "path" : "root.person.firstname",
	 "description":"Person's first name",
	 "type":"string",
	 "format":"",
	 "rules" : "split at first space, take first part",
	 "notes" : ""
	},
	....
]}
```

All of the above is done once. It will cost a lot of time, but the result is a very flexible and easily understandable structure. 

Then, when finally generating the output, the model generates the following output (for example in JSON):
```
{
"person": {
	"firstname":"John",
	"surname":"doe"
	}
}
```

And there we have our transformed result!
