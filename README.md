# Open Data Model

Connecting the dots to have an open data exchange ecosystem

## A challenge   
Nowadays, many organizations posess data about us. Sometimes we, as individuals, would like to do something with that data. Fortunately, there are now more and more possibilities to download your data yourself and store it in a vault, wallet or other location for safe keeping. So far so good. But now things get complicated quickly. Suppose you want to reuse your downloaded data, to be able to subscribe to a service or close a contract. The required data is often requested in a different format from the format in which you stored your data. How can we ensure the data will be useable in whatever format is needed?

## A solution   
What if data providing services could describe the data format they offer; data consuming services could describe what data format they expect, and we together define the business rules needed to translate between the data formats? Then this challenge would be solved.

Easier said then done. So, let's get it done. 


### The goal:
- connect organizations and individuals and work together to create a shared open data model
- lean as much as possible on existing technologies and standards
- keep it simple and easy to understand and implement
- describe data at an elementary, attribute-based level
- group attributes of certain types, for example name and birthdate are 'personal', whereas Sesamestreet is part of an 'address' 
- map attributes from data providing services
- have business rules for processing these attributes
- map attributes to a model which data consuming services can process
- input, processing and output is testable on different levels

A visual representation of the challenge:   

![input-processing-output](input-processing-output.png "Input - processing - output example")

**Input**   
These are data providing services / data sources.

**Processing**   
Attributes are translated to the required format by applying certain business rules. Things like renaming, formatting, splitting, calculating and so on. Business rules are described in an easily understandable pseudo code. From there on, implementations can be made in many languages.

**Output**   
Data is output in the requested format (think xml, json) and structure (think hierarchy) which forms the Open Data Model. But this could also be a totally different model. This output model is then used as input for a data consuming service.
